package com.laundry.reservation.laundryreservation.domain;

public class Discount {
    private Long discountId;
    private Double originalAmount;
    private Double discountedAmount;

    public Discount(){}

    public Discount(Long discountId, Double originalAmount, Double discountedAmount) {
        this.discountId = discountId;
        this.originalAmount = originalAmount;
        this.discountedAmount = discountedAmount;
    }

    public Long getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Long discountId) {
        this.discountId = discountId;
    }

    public Double getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(Double originalAmount) {
        this.originalAmount = originalAmount;
    }

    public Double getDiscountedAmount() {
        return discountedAmount;
    }

    public void setDiscountedAmount(Double discountedAmount) {
        this.discountedAmount = discountedAmount;
    }
}
