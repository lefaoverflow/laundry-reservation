package com.laundry.reservation.laundryreservation.domain;

import com.laundry.reservation.laundryreservation.builder.MachineDevicesBuilder;

public class PaymentReceipt {

    private Long receiptId;
    private LaundryReservation laundryReservation;
    private Double paymentAmount;
    private PaymentMethod paymentMethod;
    private Boolean valid;
    private String status;

    public PaymentReceipt(){
        setReceiptId();
    }

    public PaymentReceipt(Long receiptId, LaundryReservation laundryReservation, Double paymentAmount, PaymentMethod paymentMethod, Boolean valid, String status) {
        this.receiptId = receiptId;
        this.laundryReservation = laundryReservation;
        this.paymentAmount = paymentAmount;
        this.paymentMethod = paymentMethod;
        this.valid = valid;
        this.status = status;
        setReceiptId();
    }

    public Long getReceiptId() {
        return receiptId;
    }

    private void setReceiptId() {
        this.receiptId = Long.valueOf(MachineDevicesBuilder.buildRandomNumber());
    }

    public LaundryReservation getLaundryReservation() {
        return laundryReservation;
    }

    public void setLaundryReservation(LaundryReservation laundryReservation) {
        this.laundryReservation = laundryReservation;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
