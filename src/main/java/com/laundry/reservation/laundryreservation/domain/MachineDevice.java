package com.laundry.reservation.laundryreservation.domain;

import org.joda.time.DateTime;

public class MachineDevice {

    private Long machineNumber;
    private Long reservationId;
    private Boolean isReserved;
    private DateTime reservationDateTime;

    public MachineDevice(){}

    public MachineDevice(Long machineNumber, Long reservationId, Boolean isReserved, DateTime reservationDateTime) {
        this.machineNumber = machineNumber;
        this.reservationId = reservationId;
        this.isReserved = isReserved;
        this.reservationDateTime = reservationDateTime;
    }

    public Long getMachineNumber() {
        return machineNumber;
    }

    public void setMachineNumber(Long machineNumber) {
        this.machineNumber = machineNumber;
    }

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }

    public Boolean getReserved() {
        return isReserved;
    }

    public void setReserved(Boolean reserved) {
        isReserved = reserved;
    }

    public DateTime getReservationDateTime() {
        return reservationDateTime;
    }

    public void setReservationDateTime(DateTime reservationDateTime) {
        this.reservationDateTime = reservationDateTime;
    }
}
