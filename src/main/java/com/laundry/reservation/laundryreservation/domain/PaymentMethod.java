package com.laundry.reservation.laundryreservation.domain;

public enum PaymentMethod {
    CARD,
    CASH
}
