package com.laundry.reservation.laundryreservation.domain;

public class LaundryReservation {

    private Long machineNumber;
    private Long reservationId;
    private Long pin;
    private Boolean isValid;
    private String status;

    public LaundryReservation() {}

    public LaundryReservation(Long machineNumber, Long reservationId, Long pin, Boolean isValid, String status) {
        this.machineNumber = machineNumber;
        this.reservationId = reservationId;
        this.pin = pin;
        this.isValid = isValid;
        this.status = status;
    }

    public Boolean getValid() {
        return isValid;
    }

    public void setValid(Boolean valid) {
        isValid = valid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getMachineNumber() {
        return machineNumber;
    }

    public void setMachineNumber(Long machineNumber) {
        this.machineNumber = machineNumber;
    }

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }

    public Long getPin() {
        return pin;
    }

    public void setPin(Long pin) {
        this.pin = pin;
    }
}
