package com.laundry.reservation.laundryreservation.config;

import com.laundry.reservation.laundryreservation.client.Impl.NumbersAPIClientImpl;
import com.laundry.reservation.laundryreservation.client.NumbersAPIClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class ApplicationConfiguration {

    private String numbersApiUrl = "http://numbersapi.com";

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public NumbersAPIClient numbersAPIClient() {
        return new NumbersAPIClientImpl(numbersApiUrl, new RestTemplate());
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.laundry.reservation.laundryreservation.controller"))
                .paths(PathSelectors.ant("/api/v1/laundry-reservation/*"))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Laundry Reservation API",
                "Laundry Reservation Rest API",
                "v1",
                "T&Cs Apply",
                new Contact("Lefa Mashele", "lefakie@gmail.com", "lefakie@gmail.com"),
                "MIT", "MIT", Collections.emptyList());
    }

}
