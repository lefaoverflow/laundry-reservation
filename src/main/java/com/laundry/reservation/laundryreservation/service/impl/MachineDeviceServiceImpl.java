package com.laundry.reservation.laundryreservation.service.impl;

import com.laundry.reservation.laundryreservation.builder.MachineDevicesBuilder;
import com.laundry.reservation.laundryreservation.domain.MachineDevice;
import com.laundry.reservation.laundryreservation.service.MachineDeviceService;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MachineDeviceServiceImpl implements MachineDeviceService {

    private List<MachineDevice> machineDevices = MachineDevicesBuilder.buildMachineDeviceList();

    public MachineDeviceServiceImpl() {
    }

    @Override
    public Boolean lock(String reservationId, DateTime reservationDateTime, Long machineNumber) {
        for(MachineDevice machineDevice: machineDevices) {
            if(machineDevice.getMachineNumber().equals(machineNumber) && !machineDevice.getReserved()) {
                machineDevice.setReserved(true);
                machineDevice.setReservationId(Long.valueOf(reservationId));
                machineDevice.setReservationDateTime(reservationDateTime);
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean unlock(Long reservationId, Long machineNumber) {
        for(MachineDevice machineDevice: machineDevices) {
            if(machineDevice.getReservationId().equals(reservationId) && machineDevice.getMachineNumber().equals(machineNumber)) {
                machineDevice.setReserved(false);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<MachineDevice> getMachineDevices() {
        return machineDevices;
    }
}
