package com.laundry.reservation.laundryreservation.service.impl;

import com.laundry.reservation.laundryreservation.builder.MachineDevicesBuilder;
import com.laundry.reservation.laundryreservation.domain.*;
import com.laundry.reservation.laundryreservation.service.LaundryReservationMachineService;
import com.laundry.reservation.laundryreservation.service.MachineDeviceService;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LaundryReservationMachineServiceImpl implements LaundryReservationMachineService {

    private MachineDeviceService machineDeviceService;
    private List<MachineDevice> machineDevices;
    private List<LaundryReservation> laundryReservations;

    public LaundryReservationMachineServiceImpl(MachineDeviceService machineDeviceService) {
        this.machineDeviceService = machineDeviceService;
        machineDevices = machineDeviceService.getMachineDevices();
        laundryReservations = new ArrayList<>();
    }

    @Override
    public LaundryReservation createLaundryReservation(DateTime reservationDateTime, String emailAddress, String phoneNumber) throws Exception {
        MachineDevice availableMachine = retrieveAnyAvailableMachine();
        Long reservationId = Long.valueOf(MachineDevicesBuilder.buildRandomNumber());
        if(availableMachine == null) {
            throw new Exception("All Machines Are Occupied!");
        }

        LaundryReservation laundryReservation = new LaundryReservation();
        laundryReservation.setPin(Long.valueOf(MachineDevicesBuilder.buildRandomNumber()));
        laundryReservation.setReservationId(reservationId);
        laundryReservation.setMachineNumber(retrieveAnyAvailableMachine().getMachineNumber());
        laundryReservation.setValid(true);

        machineDeviceService.lock(String.valueOf(reservationId), reservationDateTime, availableMachine.getMachineNumber());
        saveLaundryReservation(laundryReservation);
        return laundryReservation;
    }

    @Override
    public PaymentReceipt processLaundryOnlinePayment(Long reservationId, PaymentMethod paymentMethod, Double amount) throws Exception {
        if (canProcessOnlinePayment(reservationId) == null) {
            throw new Exception("Unable to proceed with payment. Reservation is not found.");
        }

        Discount discount = redeemSeasonalDiscount(amount);
        PaymentReceipt paymentReceipt = new PaymentReceipt();
        paymentReceipt.setLaundryReservation(canProcessOnlinePayment(reservationId));
        paymentReceipt.setPaymentAmount(discount.getDiscountedAmount());
        paymentReceipt.setStatus("Payment Successfully Processed");
        paymentReceipt.setValid(true);
        return paymentReceipt;
    }

    @Override
    public LaundryReservation retrieveLaundryReservation(Long machineNumber, Long pin) {
        for(LaundryReservation laundryReservation: laundryReservations) {
            if(laundryReservation.getMachineNumber().equals(machineNumber) && laundryReservation.getPin().equals(pin)) {
                return laundryReservation;
            }
        }
        return null;
    }

    @Override
    public LaundryReservation claimLaundryReservation(Long machineNumber, Long pin) throws Exception {
        if(retrieveLaundryReservation(machineNumber, pin) == null) {
            throw new Exception("Cannot Retrieve Requested Reservation");
        }

        for(LaundryReservation laundryReservation: laundryReservations) {
            if(laundryReservation.getMachineNumber().equals(machineNumber) && laundryReservation.getPin().equals(pin)) {
                laundryReservation.setValid(false);
                laundryReservation.setStatus("Reservation Redeemed");
                machineDeviceService.unlock(laundryReservation.getReservationId(), laundryReservation.getMachineNumber());
                return laundryReservation;
            }
        }
        return null;
    }

    @Override
    public LaundryReservation cancelLaundryReservation(Long reservationId) throws Exception {
        for(LaundryReservation laundryReservation: laundryReservations) {
            if(laundryReservation.getReservationId().equals(reservationId)) {
                laundryReservation.setValid(false);
                laundryReservation.setStatus("Reservation Cancelled");
                machineDeviceService.unlock(laundryReservation.getReservationId(), laundryReservation.getMachineNumber());
                return laundryReservation;
            }
        }
        return null;
    }

    @Override
    public Integer availableLaundryMachines() {
        return machineDevices.size();
    }

    private MachineDevice retrieveAnyAvailableMachine() {
        for(MachineDevice machineDevice: machineDevices) {
            if(!machineDevice.getReserved()) {
                return machineDevice;
            }
        }
        return null;
    }

    private Discount redeemSeasonalDiscount(Double amount) {
        Discount discount = new Discount();
        discount.setOriginalAmount(amount);
        discount.setDiscountedAmount(amount/2);
        discount.setDiscountId(Long.valueOf(MachineDevicesBuilder.buildRandomNumber()));
        return discount;
    }

    private void saveLaundryReservation(LaundryReservation laundryReservation) {
        laundryReservations.add(laundryReservation);
    }

    private LaundryReservation canProcessOnlinePayment(Long reservationId) {
        for(LaundryReservation laundryReservation: laundryReservations) {
            if(laundryReservation.getReservationId().equals(reservationId)) {
                return laundryReservation;
            }
        }
        return null;
    }
}
