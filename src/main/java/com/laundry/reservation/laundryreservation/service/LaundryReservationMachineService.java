package com.laundry.reservation.laundryreservation.service;

import com.laundry.reservation.laundryreservation.domain.LaundryReservation;
import com.laundry.reservation.laundryreservation.domain.PaymentMethod;
import com.laundry.reservation.laundryreservation.domain.PaymentReceipt;
import org.joda.time.DateTime;

public interface LaundryReservationMachineService {

    LaundryReservation createLaundryReservation(DateTime reservationDateTime, String emailAddress, String phoneNumber) throws Exception;

    LaundryReservation retrieveLaundryReservation(Long machineNumber, Long pin);

    LaundryReservation claimLaundryReservation(Long machineNumber, Long pin) throws Exception;

    LaundryReservation cancelLaundryReservation(Long reservationId) throws Exception;

    PaymentReceipt processLaundryOnlinePayment(Long reservationId, PaymentMethod paymentMethod, Double amount) throws Exception;

    Integer availableLaundryMachines();
}
