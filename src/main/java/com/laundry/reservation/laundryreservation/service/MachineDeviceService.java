package com.laundry.reservation.laundryreservation.service;

import com.laundry.reservation.laundryreservation.domain.MachineDevice;
import org.joda.time.DateTime;

import java.util.List;

public interface MachineDeviceService {

    Boolean lock(String reservationId, DateTime reservationDateTime, Long machineNumber);

    Boolean unlock(Long reservationId, Long machineNumber);

    List<MachineDevice> getMachineDevices();
}
