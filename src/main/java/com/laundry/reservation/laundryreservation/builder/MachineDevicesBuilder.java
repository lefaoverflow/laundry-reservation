package com.laundry.reservation.laundryreservation.builder;

import com.laundry.reservation.laundryreservation.domain.MachineDevice;

import java.util.Arrays;
import java.util.List;

public class MachineDevicesBuilder {

    private static long DEFAULT_RESERVATION_ID = 0L;

    public static List<MachineDevice> buildMachineDeviceList() {
        List<MachineDevice> machineDevices =
                Arrays.asList(new MachineDevice(Long.valueOf(buildRandomNumber()), DEFAULT_RESERVATION_ID, false, null),
                              new MachineDevice(Long.valueOf(buildRandomNumber()), DEFAULT_RESERVATION_ID, false,null),
                              new MachineDevice(Long.valueOf(buildRandomNumber()), DEFAULT_RESERVATION_ID, false, null));

        return machineDevices;
    }

    public static Integer buildRandomNumber() {
        Double randomNumber = (Math.random() * ((4343250 - 434325) + 1)) + 434325;
        return  randomNumber.intValue();
    }

}
