package com.laundry.reservation.laundryreservation.client.Impl;

import com.laundry.reservation.laundryreservation.client.NumbersAPIClient;
import org.joda.time.DateTime;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

public class NumbersAPIClientImpl implements NumbersAPIClient {

    private String baseUrl;
    private RestTemplate restTemplate;

    public NumbersAPIClientImpl(String baseUrl, RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.baseUrl = baseUrl;
    }

    @Override
    public String retrieveFactByDateOfBirth(DateTime date) throws Exception {
        return restTemplate.exchange(buildFactByDateOfBirthUrl(date), HttpMethod.GET, new HttpEntity<String>(new HttpHeaders()), String.class).getBody();
    }

    @Override
    public List<String> retrieveFactsByDateOfBirth(DateTime date, Integer numberOfFacts) throws Exception {
        List<String> factList = new ArrayList<>();
        while (factList.size() < numberOfFacts) {
            factList.add(retrieveFactByDateOfBirth(date));
        }
        return factList;
    }

    private String buildFactByDateOfBirthUrl(DateTime dateTime) throws Exception {
        if (dateTime == null) {
            throw new Exception("DateTime is undefined");
        }

        return String.format("%s/%d/%d/date", baseUrl, dateTime.getMonthOfYear(), dateTime.getDayOfMonth());
    }
}
