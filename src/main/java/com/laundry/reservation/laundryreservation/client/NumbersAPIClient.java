package com.laundry.reservation.laundryreservation.client;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

public interface NumbersAPIClient {

    String retrieveFactByDateOfBirth(DateTime date) throws Exception;

    List<String> retrieveFactsByDateOfBirth(DateTime date, Integer numberOfFacts) throws Exception;
}
