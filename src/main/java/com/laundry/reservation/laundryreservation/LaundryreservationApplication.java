package com.laundry.reservation.laundryreservation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LaundryreservationApplication {

	public static void main(String[] args) {
		SpringApplication.run(LaundryreservationApplication.class, args);
	}

}
