package com.laundry.reservation.laundryreservation.controller;

import com.laundry.reservation.laundryreservation.domain.LaundryReservation;
import com.laundry.reservation.laundryreservation.service.LaundryReservationMachineService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/laundry-reservation")
public class LaundryReservationMachineController {

    private LaundryReservationMachineService laundryReservationMachineService;

    @Autowired
    public LaundryReservationMachineController(LaundryReservationMachineService laundryReservationMachineService) {
        this.laundryReservationMachineService = laundryReservationMachineService;
    }

    @PostMapping(value = "/create", produces = "application/json")
    public LaundryReservation createLaundryReservation(@RequestParam  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) DateTime reservationDateTime,
                                                       @RequestParam String emailAddress,
                                                       @RequestParam String phoneNumber) throws Exception {
        return laundryReservationMachineService.createLaundryReservation(reservationDateTime, emailAddress, phoneNumber);
    }

    @GetMapping(value = "/retrieve", produces = "application/json")
    public LaundryReservation retrieveLaundryReservation(@RequestParam Long machineNumber,
                                                         @RequestParam Long pin) throws Exception {
        return laundryReservationMachineService.retrieveLaundryReservation(machineNumber, pin);
    }

    @GetMapping(value = "/claim", produces = "application/json")
    public LaundryReservation claimLaundryReservation(@RequestParam Long machineNumber,
                                                       @RequestParam Long pin) throws Exception {
        return laundryReservationMachineService.claimLaundryReservation(machineNumber, pin);
    }

    @GetMapping(value = "/cancel/reservation/{reservationId}", produces = "application/json")
    public LaundryReservation cancelLaundryReservation(@PathVariable Long reservationId) throws Exception {
        return laundryReservationMachineService.cancelLaundryReservation(reservationId);
    }
}
