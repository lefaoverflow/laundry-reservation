package com.laundry.reservation.laundryreservation.client;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest
class NumbersAPIClientIntegrationTest {

    private String date = "2019-11-26";

    @Autowired
    private NumbersAPIClient numbersAPIClient;

    @Test
    void retrieveFactByDateOfBirth() throws Exception {
        DateTime dateTime = new DateTime(date);
        String interestingFact = numbersAPIClient.retrieveFactByDateOfBirth(dateTime);
        Assert.assertNotNull(interestingFact);
    }

    @Test
    void retrieveFactsByDateOfBirth() throws Exception {
        DateTime dateTime = new DateTime(date);
        Integer numberOfFacts = 5;

        List<String> interestingFact = numbersAPIClient.retrieveFactsByDateOfBirth(dateTime, numberOfFacts);
        Assert.assertNotNull(interestingFact);
    }
}