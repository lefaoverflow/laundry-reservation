package com.laundry.reservation.laundryreservation.service;

import com.laundry.reservation.laundryreservation.domain.LaundryReservation;
import com.laundry.reservation.laundryreservation.domain.PaymentMethod;
import com.laundry.reservation.laundryreservation.domain.PaymentReceipt;
import com.laundry.reservation.laundryreservation.service.LaundryReservationMachineService;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LaundryReservationServiceIntegrationTests {

    private DateTime reservationDateTime = new DateTime();
    private String emailAddress = "justATest@gmail.com";
    private String phoneNumber = "0115014564";

    @Autowired
    private LaundryReservationMachineService laundryReservationMachineService;

	@Test
	void shouldCreateReservation() throws Exception {
        LaundryReservation laundryReservation = laundryReservationMachineService.createLaundryReservation(reservationDateTime, emailAddress, phoneNumber);
        Assert.assertNotNull(laundryReservation);
        Assert.assertNotNull(laundryReservation.getReservationId());
        cancelReservationAfterTestRuns(laundryReservation.getReservationId());
	}

    @Test
    void shouldRetrieveReservation() throws Exception {
        LaundryReservation laundryReservationCreationResponse = laundryReservationMachineService.createLaundryReservation(reservationDateTime, emailAddress, phoneNumber);
        LaundryReservation laundryReservation = laundryReservationMachineService.retrieveLaundryReservation(laundryReservationCreationResponse.getMachineNumber(), laundryReservationCreationResponse.getPin());
        Assert.assertEquals(laundryReservation.getReservationId(), laundryReservationCreationResponse.getReservationId());
        cancelReservationAfterTestRuns(laundryReservationCreationResponse.getReservationId());
    }

    @Test
    void shouldClaimReservation() throws Exception {
        LaundryReservation laundryReservationCreationResponse = laundryReservationMachineService.createLaundryReservation(reservationDateTime, emailAddress, phoneNumber);
        LaundryReservation laundryReservationClaim = laundryReservationMachineService.claimLaundryReservation(laundryReservationCreationResponse.getMachineNumber(), laundryReservationCreationResponse.getPin());
        Assert.assertFalse(laundryReservationClaim.getValid());
        Assert.assertEquals("Reservation Redeemed", laundryReservationClaim.getStatus());
    }

    @Test
    void shouldCancelReservation() throws Exception {
        LaundryReservation laundryReservationCreationResponse = laundryReservationMachineService.createLaundryReservation(reservationDateTime, emailAddress, phoneNumber);
        LaundryReservation laundryReservationClaim = laundryReservationMachineService.cancelLaundryReservation(laundryReservationCreationResponse.getReservationId());
        Assert.assertFalse(laundryReservationClaim.getValid());
        Assert.assertEquals("Reservation Cancelled", laundryReservationClaim.getStatus());
    }

    @Test
    void shouldGetNumberOfAvailableLaundryMachines() {
	    Integer availableMachines = laundryReservationMachineService.availableLaundryMachines();
        Assert.assertEquals(java.util.Optional.of(3).get(), availableMachines);
    }

    @Test
    void shouldProcessOnlinePayment() throws Exception {
        Long reservationId = laundryReservationMachineService.createLaundryReservation(reservationDateTime, emailAddress, phoneNumber).getReservationId();
        PaymentMethod paymentMethod = PaymentMethod.CARD;
        Double amount = 250.00;

        PaymentReceipt paymentReceipt = laundryReservationMachineService.processLaundryOnlinePayment(reservationId, paymentMethod, amount);
        Assert.assertNotNull(paymentReceipt);
        Assert.assertEquals(reservationId, paymentReceipt.getLaundryReservation().getReservationId());
        Assert.assertEquals(java.util.Optional.of(125.00).get(), paymentReceipt.getPaymentAmount());
    }

    private void cancelReservationAfterTestRuns(Long reservationId) throws Exception {
        laundryReservationMachineService.cancelLaundryReservation(reservationId);
    }
}
