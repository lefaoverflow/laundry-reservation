package com.laundry.reservation.laundryreservation.controller;

import com.laundry.reservation.laundryreservation.domain.LaundryReservation;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class LaundryReservationMachineControllerIntegrationTest {

    @Autowired
    private LaundryReservationMachineController laundryReservationMachineController;

    private DateTime reservationDateTime = new DateTime();
    private String emailAddress = "justATest@gmail.com";
    private String phoneNumber = "0115014564";

    @Test
    void createLaundryReservation() throws Exception {
        LaundryReservation laundryReservation = laundryReservationMachineController.createLaundryReservation(reservationDateTime, emailAddress, phoneNumber);
        Assert.assertNotNull(laundryReservation);
        Assert.assertNotNull(laundryReservation.getReservationId());
    }
}